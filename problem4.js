function groupUsersByProgramming(users) {
  let userGroups = {
    Python: [],
    Javascript: [],
    Golang: []
  };
  //For every users check their designation matches with any programming in the usergroups
  for (const name in users) {
    for (const programName in userGroups) {
      if (users[name].desgination.includes(programName)) {
        userGroups[programName].push(name);
      }
    }
  }

  return userGroups;
}
module.exports = groupUsersByProgramming;
