function findUsersInGermany(users) {
  let usersFromGermany = [];
  //Every users check their nationality matches with Germany
  for (const key in users) {
    if (users[key].nationality.toLowerCase() === "germany") {
      usersFromGermany.push(key);
    }
  }

  return usersFromGermany;
}
module.exports = findUsersInGermany;
