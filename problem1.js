function findUserInterestedInVideoGames(users) {
  let usersInterestedInVideoGames = [];

  //For every users check their interests matches with video games
  for (const key in users) {
    let currentUserInterest = users[key].interests;
    for (let index = 0; index < currentUserInterest.length; index++) {
      if (currentUserInterest[index].includes("Video Games")) {
        usersInterestedInVideoGames.push(key);
      }
    }
  }

  return usersInterestedInVideoGames;
}
module.exports = findUserInterestedInVideoGames;
