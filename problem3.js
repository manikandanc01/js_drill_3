function findUsersHaveMasters(users) {
  let usersHavingMasters = [];
  //For every users check their qualification matches with masters
  for (const key in users) {
    if (users[key].qualification.toLowerCase() === "masters") {
      usersHavingMasters.push(key);
    }
  }

  return usersHavingMasters;
}
module.exports = findUsersHaveMasters;
